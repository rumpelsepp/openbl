// TODO: Do not hardcode table number
// TODO: Use logging methods from logging.go
// TODO: Return error, to be able to recover from errors properly

package main

import (
	"log"
	"os/exec"
	"regexp"
)

func ipfwPrepare() {
	// Scan if there is already a firewall table.
	out, err := exec.Command("ipfw", "list").Output()
	if err != nil {
		log.Fatal("ipfw command failed!")
	}

	re := regexp.MustCompile(`deny ip from table\(5\) to me`)
	if re.Match(out) == false {
		err = exec.Command("ipfw", "-q", "add", "0005", "deny", "ip", "from", "table(5)", "to", "me").Run()
		if err != nil {
			log.Fatal("Adding deny rule for table 5 failed!")
		}
	}

	err = exec.Command("ipfw", "-qf", "table", "5", "flush").Run()
	if err != nil {
		log.Fatal("Flushing table failed!")
	}
}

func ipfwBlockIP(ipAddr string) {
	if debug {
		log.Printf("[IPFW] Block: %s", ipAddr)
	}

	err := exec.Command("ipfw", "-qf", "table", "5", "add", ipAddr).Run()
	if err != nil {
		log.Fatal("Something went wrong while adding ip to firewall!")
	}
}
