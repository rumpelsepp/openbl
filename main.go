package main

import (
	"flag"
	"log"
)

var debug = false

func mainLoop() {
	log.Println("Starting mainLoop().")

	nWorker := 0
	ipAddresses := make(chan string, 100000)
	done := make(chan bool)

	PrepareFirewall()

	// Fetch, parse and validate blacklists
	for _, link := range defaultBlacklists {
		if debug {
			log.Println("Starting fetch worker.")
		}
		nWorker += 1
		go fetchBlacklist(link, ipAddresses, done)
	}

	// Receive ip strings from channel; afterwards, add them to the
	// appropriate firewall tables.
	i := 0
	for nWorker > 0 {
		select {
		case ipAddr := <-ipAddresses:
			BlockIP(ipAddr)
		case <-done:
			if debug {
				log.Println("A fetch worker is done.")
			}
			nWorker -= 1
		}

		i++
	}

	log.Printf("Added %d addresses to firewall using %s backend.", i, firewallBackend)
	log.Println("Finished.")
}

func main() {
	flag.BoolVar(&debug, "debug", false, "Enable debug logging.")
	flag.Parse()

	mainLoop()
}
