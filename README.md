# openbl

This is a remake of my openbl.org [Python
script](https://blog.sevenbyte.org/2016/08/03/use-ssh-blacklists.html). It
is about seven times faster than the Python implementation on FreeBSD. It
also, does not create wrong firewall tables any more.

## What's this?

A program that downloads a list of ip addresses from so called blacklists. These
blacklists collect ip addresses which have been suspicious due to  detected ssh,
ftp, ... break in attemps. In order to benefit from such  blacklists
([openbl.org](https://www.openbl.org/),
[spamhouse](https://www.spamhaus.org/drop/edrop.txt)) a program is needed that
downloads those lists, *validates* the downloaded strings and adds them to an
appropriate firewall table. This go progam, `openbl`, does exactly this.

## Usage

```
# export GOPATH="$HOME/Go"
# go get -u github.com/rumpelsepp/openbl
# ~/Go/bin/openbl
2016/10/24 17:07:38 Starting mainLoop().
2016/10/24 17:08:34 Added 25969 addresses to firewall using ipfw backend.
2016/10/24 17:08:34 Finished.

```

## Supported platforms

Currently there is only FreeBSD's `ipfw` firewall supported. Other firewall
backends can be added easily. Just look at the (rather short) source code!

## A note from me

This project emerged because I wanted to learn Go. If there is anything in the
code that looks messy, then please let me know! :)
