package main

import (
	"log"
	"runtime"
)

var firewallBackend string

func init() {
	switch runtime.GOOS {
	case "freebsd":
		firewallBackend = "ipfw"
	default:
		firewallBackend = "dummy"
	}
}

func PrepareFirewall() {
	switch firewallBackend {
	case "ipfw":
		ipfwPrepare()
	case "dummy":
		dummyPrepare()
	default:
		log.Panic("Firewall backend not supported!")
	}
}

func BlockIP(ipAddr string) {
	switch firewallBackend {
	case "ipfw":
		ipfwBlockIP(ipAddr)
	case "dummy":
		dummyBlockIP(ipAddr)
	default:
		log.Panic("Firewall backend not supported!")
	}
}
