package main

import "log"

func dummyPrepare() {
	log.Println("Using dummy firewall backend; not creating any rules!")
	return
}

func dummyBlockIP(ipAddr string) {
	if debug {
		log.Printf("[DUMMY] Block: %s", ipAddr)
	}
	return
}
